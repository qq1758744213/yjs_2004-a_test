package com.bwie.search.feign;

import com.bwie.model.SearchItem;
import com.bwie.search.feign.Ipml.ProductFeignClientIpml;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(value = "exam5-service-product",fallback = ProductFeignClientIpml.class)
public interface ProductFeignClient {

    @PostMapping("/product/findSearchItemList")
    public List<SearchItem> findSearchItemList(@RequestBody Map searchMap);


    }
