package com.bwie.search.service;

import com.alibaba.fastjson.JSON;
import com.bwie.model.SearchItem;
import com.bwie.search.feign.ProductFeignClient;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ESSyncService {

    @Autowired
    ProductFeignClient productFeignClient;

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Value("${exam5.elasticsearch.index}")
    private String index;

    @Autowired
    RedisTemplate redisTemplate;

    @Scheduled(cron = "0/10 * * * * * ")
    public void syncDB_ES() throws IOException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        System.out.println("进入DB2ES 定时器 " + simpleDateFormat.format(new Date()));

        String lastSyncTime = (String)redisTemplate.opsForValue().get("lastSyncTime");

        Map searchMap = new HashMap<>();

        if(!StringUtils.isEmpty(lastSyncTime)){
            searchMap.put("lastSyncTime",lastSyncTime);// 传 时间--->增量
        }

        List<SearchItem> searchItemList = productFeignClient.findSearchItemList(searchMap);

        BulkRequest bulkRequest = new BulkRequest();//批量查询

        searchItemList.stream().forEach((searchItem)->{

            IndexRequest indexRequest = new IndexRequest(index);

            indexRequest.id(String.valueOf(searchItem.getId()));

            String jsonString = JSON.toJSONString(searchItem);

            indexRequest.source(jsonString,XContentType.JSON);

            try {
                restHighLevelClient.index(indexRequest,RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        if (bulkRequest.numberOfActions()>0){
            System.out.println("有增量:" + bulkRequest.numberOfActions());
            BulkResponse bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            //System.out.println("bulkResponse" + bulkResponse.status());
        }else {
            System.out.println("没有增量");
        }

        //更新最后同步时间
        redisTemplate.opsForValue().set("lastSyncTime",simpleDateFormat.format(new Date()));
    }

}
