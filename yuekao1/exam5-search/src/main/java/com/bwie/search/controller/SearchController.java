package com.bwie.search.controller;


import com.bwie.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("search")
public class SearchController {

    @Autowired
    SearchService searchService;

    @PostMapping("search/{pageNo}/{pageSize}")
    public Map search(@PathVariable Integer pageNo,
                      @PathVariable Integer pageSize,
                      @RequestBody Map searchMap) throws IOException {

        return searchService.search(pageNo,pageSize,searchMap);
    }

}
