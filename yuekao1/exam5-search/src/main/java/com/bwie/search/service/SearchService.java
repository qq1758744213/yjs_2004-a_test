package com.bwie.search.service;

import com.alibaba.fastjson.JSON;
import com.bwie.model.SearchItem;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SearchService {

    @Value("${exam5.elasticsearch.index}")
    private String index;


   @Autowired
    RestHighLevelClient restHighLevelClient;
    /**
     * GET exam5_product/_search
     * {
     *   "from": 0,
     *   "size": 20,
     *   "query": {
     *     "bool": {
     *       "must": [
     *         {
     *           "multi_match": {
     *             "query": "快速",
     *             "fields": ["name","period"]
     *           }
     *         }
     *
     *       ],
     *       "filter": [
     *         {
     *           "term": {
     *             "status": "4"
     *           }
     *         },
     *          {
     *           "range": {
     *             "maxMoney": {
     *               "gte": 1,
     *               "lte": 2
     *             }
     *           }
     *         }
     *       ]
     *     }
     *   },
     *   "sort": [
     *     {
     *       "maxMoney": {
     *         "order": "desc"
     *       }
     *     }
     *   ],
     *   "highlight": {
     *     "pre_tags": "<strong style='color:red'>",
     *     "post_tags": "</strong>",
     *     "fields": [
     *       {
     *         "name":{}
     *       }]
     *
     *   }
     * }
     * @param searchMap
     * @return
     */
    public Map search( Integer pageNo,
                       Integer pageSize,
                       Map searchMap) throws IOException {

        String keyword = (String)searchMap.get("goodsname");
        String min = (String)searchMap.get("min");
        String max = (String)searchMap.get("max");
        String productType = (String) searchMap.get("start");

        SearchRequest searchRequest = new SearchRequest(index);//POST
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();//{
        searchSourceBuilder.from((pageNo-1)*pageSize);//from
        searchSourceBuilder.size(pageSize);//size

        {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();//query--->bool
            if(!StringUtils.isEmpty(keyword)) {
                MultiMatchQueryBuilder multiMatchQueryBuilder =
                        QueryBuilders.multiMatchQuery(keyword, "goodsname", "period");
                boolQueryBuilder.must(multiMatchQueryBuilder);//query--->bool --->must
            }
            if(!StringUtils.isEmpty(min) || !StringUtils.isEmpty(max)){
                RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("price");
                if(!StringUtils.isEmpty(min)){
                    rangeQueryBuilder.gte(min);
                }
                if(!StringUtils.isEmpty(max)) {
                    rangeQueryBuilder.lte(max);
                }
                boolQueryBuilder.filter(rangeQueryBuilder);//query--->bool --->filter -->range
            }
            if(!StringUtils.isEmpty(productType)){
                TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("start",productType);
                boolQueryBuilder.filter(termQueryBuilder);//query--->bool --->filter -->term
            }

            searchSourceBuilder.query(boolQueryBuilder);//query
        }
        searchSourceBuilder.sort("price", SortOrder.DESC);//sort

        {
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.preTags("<strong style='color:red'>");
            highlightBuilder.postTags("</strong>");
            highlightBuilder.field("goodsname");
            searchSourceBuilder.highlighter(highlightBuilder);//highlight
        }
        //聚合查询
        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders
                .terms("product_type_2004")
                .field("period");
        searchSourceBuilder.aggregation(termsAggregationBuilder);//aggs

        searchRequest.source(searchSourceBuilder);//}

        //发送请求
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);//

        //结果解析
        //hits
        SearchHits hits = searchResponse.getHits();
        long total = hits.getTotalHits().value;//
        SearchHit[] hits1 = hits.getHits();//结果集
        List<SearchItem> searchItemList = new ArrayList<>();
        for (SearchHit hit: hits1){

            String sourceAsString_json = hit.getSourceAsString();
            SearchItem searchItem = JSON.parseObject(sourceAsString_json, SearchItem.class);
            //高亮结果的处理
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField highlightField = highlightFields.get("goodsname");
            if(highlightField!=null){
                Text[] fragments = highlightField.getFragments();
                if(fragments.length>0){
                    String highlight_name = fragments[0].string();
                    searchItem.setGoodsname(highlight_name);
                }
            }
            searchItemList.add(searchItem);
        }

        //聚合查询 map
        Map map_product_type = new HashMap();
        Aggregations aggregations = searchResponse.getAggregations();
        ParsedStringTerms product_type_2004 = aggregations.get("product_type_2004");//ParsedStringTerms
        List<? extends Terms.Bucket> buckets = product_type_2004.getBuckets();
        for(Terms.Bucket bucket:buckets){
            String keyAsString = bucket.getKeyAsString();
            long docCount = bucket.getDocCount();
            map_product_type.put(keyAsString,docCount);
        }
        Map map = new HashMap();
        map.put("total",total);
        map.put("searchItemList",searchItemList);
        map.put("product_type",map_product_type);
        return map;

    }
}
