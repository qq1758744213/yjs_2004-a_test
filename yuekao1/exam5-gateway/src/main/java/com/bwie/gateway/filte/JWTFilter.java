package com.bwie.gateway.filte;

import com.bwie.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

@Component
public class JWTFilter implements GlobalFilter, Ordered {

    @Autowired
    RedisTemplate redisTemplate;

    private String[] whiteArray = new String[]{
            "login","uploadOss","uploadFastDFS"
    };

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String path = request.getURI().getPath();
        for (String white:whiteArray){
            if (path.contains(white)){
             return chain.filter(exchange);//放行
            }
        }
        String authorization = request.getHeaders().getFirst("Authorization");
        if (StringUtils.isEmpty(authorization)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        String token = authorization.replace("Bearer ", "");
        if (StringUtils.isEmpty(authorization)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        try {
            Claims jwt_body = JwtUtils.getJwt_Body(token);
           String userid = (String)jwt_body.get("userid");
            String username = (String)jwt_body.get("username");
            String token_redis  = (String) redisTemplate.opsForValue().get("token:" + userid);
            if (!StringUtils.isEmpty(token_redis)){
                redisTemplate.opsForValue().set("token:"+userid,token,2, TimeUnit.MINUTES);
                return chain.filter(exchange);
            }else {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
        } catch (Exception e) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
    }

    @Override
    public int getOrder() {
        return -100;
    }
}
