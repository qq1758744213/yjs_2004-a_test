package com.bwie.exam5.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ResultObject<T> implements Serializable {

    private boolean success;

    private String code;//返回的业务码 1：成功执行 0：发生错误，其他自定义
    private String message;
    private T object;

}
