package com.bwie.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName("tb_goods")
@Data
public class GoodsEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    public Integer id;

    private Long period;

    private BigDecimal price;

    private Date createTime;

    private String goodNum;

    private String goodsType;

    private BigDecimal onPrice;

    private Integer haveMuch;

    private String goodsname;

    private Integer start;

    private Date updateTime;

}
