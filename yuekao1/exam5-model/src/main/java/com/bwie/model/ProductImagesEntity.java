package com.bwie.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_product_images")
public class ProductImagesEntity implements Serializable {


    @TableId(type = IdType.AUTO)
    private Integer id;

    private String url;

    private Integer productId;//

    private String bakUrl;
}
