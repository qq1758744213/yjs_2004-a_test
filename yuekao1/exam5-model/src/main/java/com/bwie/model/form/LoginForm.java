package com.bwie.model.form;

import lombok.Data;

@Data
public class LoginForm {

    private String loginName;

    private String password;
}
