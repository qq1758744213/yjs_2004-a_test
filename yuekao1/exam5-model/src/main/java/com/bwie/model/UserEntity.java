package com.bwie.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_user")
public class UserEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String loginName;

    private String password;

    private String mobile;

    private String type;//
}
