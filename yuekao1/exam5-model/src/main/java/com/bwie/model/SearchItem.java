package com.bwie.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SearchItem {

    public Integer id;

    private String period;

    private BigDecimal price;

    private Date createTime;

    private String goodNum;

    private String goodsType;

    private BigDecimal onPrice;

    private Integer haveMuch;

    private String goodsname;

    private Integer start;

    private String updateTime;

    private String mainPic;
}
