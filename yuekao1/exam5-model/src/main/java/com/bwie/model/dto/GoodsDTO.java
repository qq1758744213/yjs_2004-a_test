package com.bwie.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.bwie.model.ProductImagesEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class GoodsDTO {

    public Integer id;

    private String period;

    private BigDecimal price;

    private Date createTime;

    private String goodsNum;

    private String goodsType;

    private BigDecimal onPrice;

    private String goodsname;

    private Integer haveMuch;

    private Integer start;//1上架 /2下架

    private BigDecimal min;

    private BigDecimal max;

    private Date updateTime;

    List<ProductImagesEntity> imagesList;
}
