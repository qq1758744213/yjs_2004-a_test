package com.bwie.product.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bwie.exam5.common.Result;
import com.bwie.model.DictEntity;
import com.bwie.model.GoodsEntity;
import com.bwie.model.SearchItem;
import com.bwie.model.dto.GoodsDTO;
import com.bwie.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    ProductService productService;
    //添加
    @PostMapping("save")
    public Result save(@RequestBody GoodsDTO goodsDTO){
       return productService.save(goodsDTO);
    }

    @PostMapping("findPage/{pageNo}/{pageSize}")
    public Page<GoodsEntity> findPage(@PathVariable Integer pageNo,
                                      @PathVariable Integer pageSize){
      return productService.findPage(pageNo,pageSize);
    }
    /*
        es添加
     */
    @PostMapping("findSearchItemList")
    public List<SearchItem> findSearchItemList(@RequestBody Map searchMap){
        return productService.findSearchItemList(searchMap);
    }

    /**
     * 二级两栋
     * @param pid
     * @return
     */
    @GetMapping("category/findChildrenByParentId/{pid}")
    public List<DictEntity> findChildrenByParentId(@PathVariable Long pid){
        return productService.findChildrenByParentId(pid);
    }
    //提交方法
    @GetMapping("sumbit/{id}")
    public Result sumbit(@PathVariable Integer id){
        return productService.sumbit(id);
    }
    //下架方法
    @GetMapping("sumbit1/{id}")
    public Result sumbit1(@PathVariable Integer id){
        return productService.sumbit(id);
    }

}
