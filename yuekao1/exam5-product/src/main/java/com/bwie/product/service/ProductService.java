package com.bwie.product.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bwie.exam5.common.Result;
import com.bwie.model.DictEntity;
import com.bwie.model.GoodsEntity;
import com.bwie.model.ProductImagesEntity;
import com.bwie.model.SearchItem;
import com.bwie.model.dto.GoodsDTO;
import com.bwie.product.feign.DictFeignClient;
import com.bwie.product.mapper.ProductImagesMapper;
import com.bwie.product.mapper.ProductMapper;
import com.bwie.utils.IdWorker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    ProductMapper productMapper;

    @Autowired
    ProductImagesMapper productImagesMapper;

    @Autowired
    DictFeignClient dictFeignClient;

    @Transactional//事务
    public Result save(GoodsDTO goodsDTO) {

        String goodsNum = goodsDTO.getGoodsNum();

        QueryWrapper<GoodsEntity> queryWrapper = new QueryWrapper<>();

        queryWrapper.lambda().eq(GoodsEntity::getGoodNum,goodsNum);

        List<GoodsEntity> goodsEntities = productMapper.selectList(queryWrapper);

        if (goodsEntities.size()>0){
            return new Result(false,"0","单号重复");
        }
        GoodsEntity goodsEntity = new GoodsEntity();

        BeanUtils.copyProperties(goodsDTO,goodsEntity);

        goodsEntity.setCreateTime(new Date());

        goodsEntity.setStart(1);

        productMapper.insert(goodsEntity);

        List<ProductImagesEntity> imagesList = goodsDTO.getImagesList();

        imagesList.stream().forEach((imagesEntity)->{
            imagesEntity.setProductId(goodsEntity.id);
            productImagesMapper.insert(imagesEntity);
        });

        return new Result(true,"1","添加成功");
    }

    public Page<GoodsEntity> findPage(Integer pageNo, Integer pageSize) {

        Page<GoodsEntity> page = new Page<>(pageNo, pageSize);
        return productMapper.selectPage(page, Wrappers.emptyWrapper());
    }

    public List<SearchItem> findSearchItemList(Map searchMap) {
        String lastSyncTime = (String)searchMap.get("lastSyncTime");

        QueryWrapper<GoodsEntity> queryWrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(lastSyncTime)){//增量
            queryWrapper.lambda().gt(GoodsEntity::getUpdateTime,lastSyncTime);
        }else {
            //全量
        }

        List<GoodsEntity> productEntities = productMapper.selectList(queryWrapper);

        return productEntities.stream().map((productEntity)->{
            SearchItem searchItem = new SearchItem();
            BeanUtils.copyProperties(productEntity,searchItem);
            switch (productEntity.getPeriod()){
                case "1" :
                    searchItem.setPeriod("百货");
                    break;
                case "2" :
                    searchItem.setPeriod("男装");
                    break;
                case "3" :
                    searchItem.setPeriod("女装");
                    break;
                case "4" :
                    searchItem.setPeriod("餐饮用具");
                    break;
                case "5" :
                    searchItem.setPeriod("收纳整理");
                    break;
                case "6" :
                    searchItem.setPeriod("休闲");
                    break;
                case "7" :
                    searchItem.setPeriod("运动");
                    break;
                case "8" :
                    searchItem.setPeriod("裙子");
                    break;
                case "9" :
                    searchItem.setPeriod("西装");
                    break;
            }

            Long productType = productEntity.getPeriod();
            DictEntity dictEntityProductType = (DictEntity) dictFeignClient.findDictType(productType);
            searchItem.setPeriod(dictEntityProductType.getName());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            searchItem.setUpdateTime(simpleDateFormat.format(productEntity.getUpdateTime()));
            return searchItem;
        }).collect(Collectors.toList());
    }

    @Autowired
    RedisTemplate redisTemplate;
    public List<DictEntity> findChildrenByParentId(Long pid) {
        List<DictEntity> productTypeList_redis = (List<DictEntity>)redisTemplate.opsForValue().get("ProductTypeList");
        if(productTypeList_redis!=null){
            System.out.println("从Redis中获取productTypeList");
            return productTypeList_redis;
        }else{
            List<DictEntity> productTypeList = dictFeignClient.findDictType(pid);
            redisTemplate.opsForValue().set("ProductTypeList",productTypeList);
            System.out.println("redis中没有，查询数据库，然后装入redis");
            return productTypeList;
        }
    }

    @Autowired
    RabbitTemplate rabbitTemplate;
    public Result sumbit(Integer id) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setId(id);
        goodsEntity.setStart(1);
        productMapper.updateById(goodsEntity);
        goodsEntity.setUpdateTime(new Date());
        /**
         * String exchange, 交换机
         * String routingKey,
         * Object message, 消息
         * MessagePostProcessor messagePostProces
         */
        rabbitTemplate.convertAndSend(
                "","Queue_audit","请求CEO审核",
                (message) -> {
                    IdWorker idWorker = new IdWorker();
                    long messageId= idWorker.nextId();
                    message.getMessageProperties().setMessageId(String.valueOf(messageId));
                    return message;
                }
        );
        return new Result(true,"1","修改成功");
    }
    public Result sumbit1(Integer id) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setId(id);
        goodsEntity.setStart(0);
        productMapper.updateById(goodsEntity);
        /**
         * String exchange, 交换机
         * String routingKey,
         * Object message, 消息
         * MessagePostProcessor messagePostProces
         */
        rabbitTemplate.convertAndSend(
                "","Queue_audit","请求CEO审核",
                (message) -> {
                    IdWorker idWorker = new IdWorker();
                    long messageId= idWorker.nextId();
                    message.getMessageProperties().setMessageId(String.valueOf(messageId));
                    return message;
                }
        );
        return new Result(true,"1","修改成功");
    }
}
