package com.bwie.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwie.model.GoodsEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductMapper extends BaseMapper<GoodsEntity> {
}
