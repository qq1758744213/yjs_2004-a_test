package com.bwie.product.config;

import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.fdfs.ThumbImageConfig;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadFileWriter;
import com.github.tobato.fastdfs.exception.FdfsUnsupportStorePathException;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *   <dependency>
 *     <groupId>com.github.tobato</groupId>
 *     <artifactId>fastdfs-client</artifactId>
 *     <version>1.26.5</version>
 * </dependency>
 *
 */

@Component
public class FastDFSClient {
    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsWebServer fdfsWebServer;

    /**
     * 通过HTTP上传文件
     * @param file
     * @return
     */
    public String upload(MultipartFile file){
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(
                    file.getInputStream(),/*InputStream*/
                    file.getSize(),
                    FilenameUtils.getExtension(file.getOriginalFilename()),
                    createMetaData());
            return getResAccessUrl(storePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Autowired
    private ThumbImageConfig thumbImageConfig;
    /**
     * 上传缩略图
     * @param file
     * @return
     */
    public String uploadThumbImage(MultipartFile file){
        try {
            StorePath storePath = fastFileStorageClient.uploadImageAndCrtThumbImage(
                    file.getInputStream(),/*InputStream*/
                    file.getSize(),
                    FilenameUtils.getExtension(file.getOriginalFilename()),
                    createMetaData());

            return getResAccessUrl(storePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 封装图片完整URL地址
    private String getResAccessUrl(StorePath storePath) {
        //fdfsWebServer.getWebServerUrl()=http://47.94.97.131/
        //getFullPath = group1/M00/12/ed/文件名
        String fileUrl = fdfsWebServer.getWebServerUrl() + storePath.getFullPath();
        return fileUrl;
    }

    private String getThumbImageResAccessUrl(StorePath storePath) {
        //fdfsWebServer.getWebServerUrl()=http://47.94.97.131/
        //getFullPath = group1/M00/12/ed/文件名
        String slavePath = thumbImageConfig.getThumbImagePath(storePath.getPath());
        String fileUrl = fdfsWebServer.getWebServerUrl() + slavePath;
        return fileUrl;
    }

    private Set<MetaData> createMetaData() {
        Set<MetaData> metaDataSet = new HashSet<>();
        metaDataSet.add(new MetaData("Author", "tobato"));
        metaDataSet.add(new MetaData("CreateDate", "2016-01-05"));
        return metaDataSet;
    }

    /**
     * 上传本地文件
     * @param file
     * @return
     * @throws IOException
     */
    public String upload(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        StorePath storePath = fastFileStorageClient.uploadFile(
                inputStream,
                file.length(),
                FilenameUtils.getExtension(file.getAbsolutePath()),
                null);
        //System.out.println(storePath.getGroup());
        //System.out.println(storePath.getPath());
        System.out.println(storePath.getFullPath());

        return storePath.getFullPath();
    }

    /**
     * 下载文件到本地
     * @param fileUrl
     * @param localFileName
     */
    public void download(String fileUrl,String localFileName){
        StorePath storePath = StorePath.parseFromUrl(fileUrl);
        fastFileStorageClient.downloadFile(
                storePath.getGroup(),
                storePath.getPath(),
                new DownloadFileWriter(localFileName)
        );
    }

    /**
     * 删除文件
     * @param fileUrl 文件访问地址
     * @return
     */
    public void deleteFile(String fileUrl) {
        if (StringUtils.isEmpty(fileUrl)) {
            return;
        }
        try {
            StorePath storePath = StorePath.parseFromUrl(fileUrl);
            fastFileStorageClient.deleteFile(storePath.getGroup(), storePath.getPath());
        } catch (FdfsUnsupportStorePathException e) {
            e.printStackTrace();
        }
    }

}
