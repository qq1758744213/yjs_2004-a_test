package com.bwie.product.config;


import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    private static final String QUEUE_NAME = "Queue_audit";


    /**
     * 队列持久化
     *
     * @return
     */
    @Bean(QUEUE_NAME)
    public Queue queueDeclare() {
        return QueueBuilder.durable(QUEUE_NAME).build();
    }
}