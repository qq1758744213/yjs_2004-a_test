package com.bwie.product.feign;

import com.bwie.model.DictEntity;
import com.bwie.product.feign.Ipml.DictFeignClientIpml;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "exam5-service-dict",fallback = DictFeignClientIpml.class)
public interface DictFeignClient {

    @GetMapping("/dict/findDictType/{pid}")
    public List<DictEntity> findDictType(@PathVariable Long pid);

}
