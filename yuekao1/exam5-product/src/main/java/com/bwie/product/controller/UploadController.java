package com.bwie.product.controller;

import com.aliyun.oss.OSS;
import com.bwie.exam5.common.Result;
import com.bwie.exam5.common.ResultObject;
import com.bwie.product.config.FastDFSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("product")
public class UploadController {

    @Autowired
    OSS ossClient;

    @Autowired
    FastDFSClient fastDFSClient;

    @Value("${oss.bucket}")
    String bucketName;

    @Value("${oss.hostName}")
    String hostName;

    @PostMapping("uploadOss")
    public ResultObject<Map> uploadOss(@RequestBody MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        ossClient.putObject(bucketName,originalFilename,file.getInputStream());
        String url = hostName + originalFilename;
        String bak_url = fastDFSClient.upload(file);
        Map map = new HashMap<>();
        map.put("url",url);
        map.put("bak_url",bak_url);
        return new ResultObject<Map>(true,"1","上传成功",map);
    }

    @PostMapping("uploadFastDFS")
    public Result uploadFastDFS(@RequestBody MultipartFile file) throws IOException {

        String url = fastDFSClient.upload(file);
        return new Result(true,"1",url);
    }
}
