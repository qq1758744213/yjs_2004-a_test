package com.bwie.product.service;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

@RabbitListener(queues = {"Queue_audit"})
@Component
public class AuditMessageListener {

    @Autowired
    RedisTemplate redisTemplate;

    @RabbitHandler
    public void revMessage(Channel channel, Message message,String msg) throws IOException {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        String messageId = message.getMessageProperties().getMessageId();
        String revMessage = (String)redisTemplate.opsForHash().get("revMessageList", messageId);
        if (StringUtils.isEmpty(revMessage)){
            channel.basicAck(deliveryTag,false);
            return;
        }else{
            System.out.println("接受到的消息="+msg);//业务逻辑

            redisTemplate.opsForHash().put("revMessageList",messageId,msg);
            channel.basicAck(deliveryTag,false);
        }

    }
}
