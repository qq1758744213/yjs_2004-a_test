package com.bwie.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwie.model.DictEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DictMapper extends BaseMapper<DictEntity> {
}
