package com.bwie.dict.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bwie.dict.mapper.DictMapper;
import com.bwie.model.DictEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictService {

    @Autowired
    DictMapper dictMapper;

    public List<DictEntity> findDictType(String pid) {

        QueryWrapper<DictEntity> queryWrapper = new QueryWrapper<>();

        queryWrapper.lambda().eq(DictEntity::getGoodsId,pid);

        List<DictEntity> dictEntities = dictMapper.selectList(queryWrapper);

        return dictEntities;
    }
}
