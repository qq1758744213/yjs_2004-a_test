package com.bwie.dict.controller;

import com.bwie.dict.service.DictService;
import com.bwie.model.DictEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

@RequestMapping("dict")
@RestController
public class DictController {

    @Autowired
    DictService dictService;

    @GetMapping("findDictType/{pid}")
    public List<DictEntity> findDictType(@PathVariable String pid){
        return dictService.findDictType(pid);
    }

}
