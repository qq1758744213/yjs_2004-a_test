package com.bwie.utils;

import io.jsonwebtoken.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

    private static final String ISSUER = "AAAAA";
    private static final long EXPIRITION = 1000 * 60 * 60 * 24  * 7;//7天
    //private static final long EXPIRITION = 1000 * 5;//7天

    private static final String APPSECRET_KEY = "abc123";

    public static final String USERNAME_CLAIMS = "username";//
    public static final String USERID_CLAIMS = "userid";
    public static final String ROLES_CLAIMS = "roles";


    /**
     * 由字符串生成加密key
     *
     * @return
     */
    public static SecretKey generalKey(){
        byte[] encodedKey = Base64.getEncoder().encode(APPSECRET_KEY.getBytes());
        SecretKeySpec key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");

        return key;
    }

    /**
     * 生成token
     * @param username 用户名
     * @param attrMap 其他的信息
     * @return
     */
    public static String createToken(String userid,
                                     String username,
                                     Map attrMap) {

        // 指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;//Header

        // 生成JWT的时间
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        Map<String,Object> map = new HashMap<>();
        map.put(USERNAME_CLAIMS, username);
        map.put(USERID_CLAIMS, userid);
        if(attrMap!=null&&attrMap.size()>0){
            map.putAll(attrMap);
        }
        // 生成签名的时候使用的秘钥secret，切记这个秘钥不能外露哦。它就是你服务端的私钥，
        // 在任何场景都不应该流露出去。
        // 一旦客户端得知这个secret, 那就意味着客户端是可以自我签发jwt了。
        SecretKey key = generalKey();

        // 下面就是在为payload添加各种标准声明和私有声明了
        /*
        iss: jwt签发者
        sub: jwt所面向的用户
        aud: 接收jwt的一方
        exp: jwt的过期时间，这个过期时间必须要大于签发时间
        nbf: 定义在什么时间之前，该jwt都是不可用的.  not before
        iat: jwt的签发时间
        jti: jwt的唯一身份标识，主要用来作为一次性token,从而回避重放攻击。
         */
        JwtBuilder builder = Jwts.builder() // 这里其实就是new一个JwtBuilder，设置jwt的body
                .setClaims(map)          // 如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
                .setId(userid)           // 设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
                .setIssuedAt(now)           // iat: jwt的签发时间
                .setIssuer(ISSUER)          // iss：jwt签发人
                .setSubject(username)        // sub(Subject)：代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。
                .signWith(signatureAlgorithm, APPSECRET_KEY); // 设置签名使用的签名算法和签名使用的秘钥

        // 设置过期时间
        if (EXPIRITION >= 0) {
            long expMillis = nowMillis + EXPIRITION;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);//exp
        }
        return builder.compact();

    }

    public static Jws<Claims> parseJwt(String token){
        SecretKey key = generalKey();
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(APPSECRET_KEY)
                .parseClaimsJws(token);
        return claimsJws;
    }

    public static Claims getJwt_Body(String token){
        Claims claims = parseJwt(token)
                .getBody();
        return claims;
    }

    /**
     * 是否过期
     * @param token
     * @return
     */
    public static boolean isExpiration(String token){
        Claims claims = getJwt_Body(token);
        System.out.println(claims.getExpiration());
        return claims.getExpiration().before(new Date());
    }

    public static void main(String[] args) {

        Map map = new HashMap();
        map.put("circle","交友");
        map.put("class","2004A");
        String jwt_token = createToken("6", "zhangsan", map);

        /*
        JWTString = Base64(Header).Base64(Payload).HMACSHA256(base64UrlEncode(header) + "." + base64UrlEncode(payload), secret)
        eyJhbGciOiJIUzI1NiJ9.   //Header 头部信息
        eyJzdWIiOiJ6aGFuZ3NhbiIsImlzcyI6IkFBQUFBIiwiY2lyY2xlIjoi5Lqk5Y-LIiwiZXhwIjoxNjUyNDEyOTQ4LCJpYXQiOjE2NTE4MDgxNDgsImp0aSI6IjYiLCJ1c2VybmFtZSI6InpoYW5nc2FuIn0.  //载荷信息
        _VaGZ4VfRrYdHc-9etdbOhIYYohKEAJGuYF40LHS988   //签名信息

        eyJhbGciOiJIUzI1NiJ9
        .eyJzdWIiOiJ6aGFuZ3NhbiIsImlzcyI6IkFBQUFBIiwiY2lyY2xlIjoi5Lqk5Y-LIiwiZXhwIjoxNjUyNDEzMzU1LCJpYXQiOjE2NTE4MDg1NTUsImp0aSI6IjYiLCJ1c2VybmFtZSI6InpoYW5nc2FuIn0
        .nvRtAP_wP0t_QdZ-rlLFCvBn9nDGe4EeHVkVo2yHgag

        eyJhbGciOiJIUzI1NiJ9
        .eyJzdWIiOiJ6aGFuZ3NhbiIsImlzcyI6IkFBQUFBIiwiY2lyY2xlIjoi5Lqk5Y-LIiwiZXhwIjoxNjUyNDEzNDY3LCJjbGFzcyI6IjE5MTFBIiwiaWF0IjoxNjUxODA4NjY3LCJqdGkiOiI2IiwidXNlcm5hbWUiOiJ6aGFuZ3NhbiJ9
        .32YSgry47CNWcFLNX91GjbYBMgfH1IEeDu_IurXZAGk


        eyJhbGciOiJIUzI1NiJ9.
        eyJzdWIiOiJ6aGFuZ3NhbiIsImlzcyI6IkFBQUFBIiwiY2lyY2xlIjoi5Lqk5Y-LIiwiZXhwIjoxNjUxODA5NTQ4LCJjbGFzcyI6IjE5MTFBIiwiaWF0IjoxNjUxODA5NTQzLCJqdGkiOiI2IiwidXNlcm5hbWUiOiJ6aGFuZ3NhbiJ9.
        I794uTVP06U1eQRZtmIDRnXFgBLU2VXLeeeb1R2MuB8

         */
        System.out.println(jwt_token);

        //
        Jws<Claims> claimsJws = parseJwt(jwt_token);

        JwsHeader header = claimsJws.getHeader();
        System.out.println(header);
        Claims body = claimsJws.getBody();
        System.out.println(body);
        String signature = claimsJws.getSignature();
        System.out.println(signature);
    }
}
