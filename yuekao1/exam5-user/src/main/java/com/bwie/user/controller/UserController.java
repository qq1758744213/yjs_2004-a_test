package com.bwie.user.controller;

import com.bwie.exam5.common.ResultObject;
import com.bwie.model.form.LoginForm;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("login")
    public ResultObject login(@RequestBody LoginForm loginForm){
        return userService.login(loginForm);
    }

}
