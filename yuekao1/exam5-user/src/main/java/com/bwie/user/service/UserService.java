package com.bwie.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bwie.exam5.common.ResultObject;
import com.bwie.model.UserEntity;
import com.bwie.model.form.LoginForm;
import com.bwie.user.mapper.UserMapper;
import com.bwie.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisTemplate redisTemplate;

    public ResultObject login(LoginForm loginForm) {

        String loginName = loginForm.getLoginName();

        String rewpassword = loginForm.getPassword();

        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();

        queryWrapper.lambda().eq(UserEntity::getLoginName,loginName);

        List<UserEntity> userEntities = userMapper.selectList(queryWrapper);

        if (userEntities.size()==0){
            return new ResultObject(false,"0","用户名或者密码错误",null);
        }
        UserEntity userEntity = userEntities.get(0);
        String password = userEntity.getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (!bCryptPasswordEncoder.matches(rewpassword,password)){
            return new ResultObject(false,"0","用户名或者密码错误",null);
        }
        Integer id = userEntity.getId();
        String token = JwtUtils.createToken(id.toString(), userEntity.getLoginName(), null);
        redisTemplate.opsForValue().set("token:"+id,token,2, TimeUnit.MINUTES);
        Map map = new HashMap();
        map.put("userid",id);
        map.put("username",userEntity.getLoginName());
        map.put("type",userEntity.getType());
        return new ResultObject(true,"1",token,map);


    }
}
