package com.bwie.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwie.model.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
}
